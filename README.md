# Data science portfolio
A sample of data science projects and coding I've authored, including a summary of the main data science subjects and Python packages used. Full code and additional detail for each at the "view project" links.

Home price estimator - [[view project]](https://bitbucket.org/logantech/home-price-estimator) |  
------------- | -------------
Data exploration; Tableau visualization; hypothesis testing; machine learning models including linear regression, SVD and PCA; feature reduction; tests for normal (QQ, KS) | [![](images/viz.png)](https://bitbucket.org/logantech/home-price-estimator)

––  

Water security in Tanzania - [[view project]](https://bitbucket.org/logantech/water-pumps) |  
------------- | -------------
Capstone project. Merging multiple datasets; exploratory data analysis; feature reduction through multiple algorithms; machine learning modeling (SVM, Logistic Regression, Random Forest); model optimization through grid search and hyperparameter tuning; performance evaluation  | [![](images/water-pumps.png)](https://bitbucket.org/logantech/water-pumps)

––  

Ad performance regression analysis - [[view project]](https://bitbucket.org/logantech/ad-regression) |  
------------- | -------------
Linear least-squares regression; goodness of fit; bootstrapping of influencers; C-level communication   | [![](images/regression.png)](https://bitbucket.org/logantech/ad-regression)

––  


Auto hypothesis testing - [[view project]](https://bitbucket.org/logantech/auto-hypothesis) |  
------------- | -------------
Tests for normal; bootstrapping for summary statistics; detecting differences in distributions; Bayesian estimation of mean and variance; Tukey's HSD   | [![](images/bayes.png)](https://bitbucket.org/logantech/auto-hypothesis)


––  

Hepatitis machine learning modeling - [[view project]](https://bitbucket.org/logantech/hepatitis) |  
------------- | -------------
Data acquisition, cleaning and validation; correlation analysis; feature analysis and engineering; exploratory data analysis; Support Vector Machine and Naive Bayes classifier models; estimating model performance  | [![](images/roc.png)](https://bitbucket.org/logantech/hepatitis)


––  

Exploratory data analysis and visualization - [[view project]](https://bitbucket.org/logantech/casino-feature-viz) |  
------------- | -------------
Data engineering; heatmaps; scatter plots; faceting; time-series analysis; trend detection; Seaborn and Matplotlib libraries | [![](images/heatmap.png)](https://bitbucket.org/logantech/casino-feature-viz)

––  

Classification machine learning - [[view project]](https://bitbucket.org/logantech/wine-detector) |  
------------- | -------------
Gaussian Naive Bayes classification modeling; feature reduction; Winsorizing, scaling and normalizing data; KFold cross-validation; PCA analysis | [![](images/separability.png)](https://bitbucket.org/logantech/wine-detector)

––  

Information system modeling   - [[view project]](https://bitbucket.org/logantech/library-data-flow) |  
------------- | -------------
Create a data flow diagram for a system to recommend books to library patrons based on their library records and book recommendations. | [![](images/data-flow-diagram.png)](https://bitbucket.org/logantech/library-data-flow)
